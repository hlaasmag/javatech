package praktikum1Ylesanded;

public class DecreasingMatrix {

	public static void main(String[] args) {
		int scope = 1;
		int valueAddedToMatrix = 9;
		int lengthOfSquareSide = 10;

		int[][] grid = new int[lengthOfSquareSide][lengthOfSquareSide];

		for (int i = 0; i < lengthOfSquareSide; i++) {
			for (int j = 0; j < scope; j++) {
				grid[i][j] = valueAddedToMatrix; // Fills the main diagonal and matrix area LEFT from the main diagonal.
				grid[j][i] = valueAddedToMatrix; // Fills the main diagonal and matrix area RIGHT from the main diagonal.
			}
			valueAddedToMatrix--;
			scope++;
		}
		printOutMatrix(lengthOfSquareSide, grid);
	}

	public static void printOutMatrix(int lengthOfSquareSide, int[][] grid) {
		for (int i = 0; i < lengthOfSquareSide; i++) {
			for (int j = 0; j < lengthOfSquareSide; j++) {
				System.out.print(String.valueOf(grid[i][j]) + " ");
			}
			System.out.println();
		}
	}

}
