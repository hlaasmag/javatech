package praktikum1Ylesanded;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class GraphNumbersByUsersSimpleVersion {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int userInput = 0;
		ArrayList<Integer> listOfUserInputs = new ArrayList<Integer>();
		do {
			System.out.println("Please enter a number!");
			userInput = scan.nextInt();
			listOfUserInputs.add(userInput);
		} while (userInput > 0);
		listOfUserInputs.remove(listOfUserInputs.size() - 1);

		int xesWritten = 0;
		for (int i = 0; i < listOfUserInputs.size(); i++) {
			System.out.print(listOfUserInputs.get(i) + "  " + "|");

			while (listOfUserInputs.get(i) - xesWritten > 0) {
				System.out.print(" X");
				xesWritten++;

			}
			xesWritten = 0;
			System.out.println();

		}
		scan.close();
	}
}
