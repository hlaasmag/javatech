package praktikum1Ylesanded;

import java.util.Scanner;

public class Digits {

	private static Scanner scanner;

	public static void main(String[] args) {
		scanner = new Scanner(System.in);
		int number = scanner.nextInt();

		System.out.println(sum(number));
		System.out.println(product(number));
	}

	public static int sum(Integer num) {
		if (num == 0 || num == null) {
			return 0;
		}

		int sumOfDigits = 0;

		String inputAsString = String.valueOf(num);
		for (int i = 0; i < inputAsString.length(); i++) {
			sumOfDigits += Character.getNumericValue((inputAsString.charAt(i)));
		}
		return sumOfDigits;
	}

	public static int product(Integer num) {
		if (num == 0 || num == null) {
			return 0;
		}

		int productOfDigits = 1;

		String inputAsString = String.valueOf(num);
		for (int i = 0; i < inputAsString.length(); i++) {
			productOfDigits *= Character.getNumericValue((inputAsString.charAt(i)));
		}
		return productOfDigits;
	}

}
