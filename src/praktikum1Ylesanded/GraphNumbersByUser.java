package praktikum1Ylesanded;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class GraphNumbersByUser {
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int userInput = 0;
		ArrayList<Integer> listOfUserInputs = new ArrayList<Integer>();
		do {
			System.out.println("Please enter a number!");
			userInput = scan.nextInt();
			listOfUserInputs.add(userInput);
		} while (userInput > 0);
		listOfUserInputs.remove(listOfUserInputs.size() - 1);

		int maxNumber = Collections.max(listOfUserInputs);

		scan.close();

		for (int i = 0; i < maxNumber; i++) { // columns
			for (int j = 0; j < listOfUserInputs.size(); j++) { // rows
				if (i < maxNumber) {
					if ((maxNumber - i) <= listOfUserInputs.get(j)) {
						System.out.print("X\t");
					} else {
						System.out.print("\t");
					}

				}

			}
			System.out.println();

		}
		System.out.println("---------------------------------------------------------------------");
		for (int n = 0; n < listOfUserInputs.size(); n++) {
			System.out.print(listOfUserInputs.get(n) + "\t");

		}

	}
}
