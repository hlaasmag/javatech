package praktikum1Ylesanded;

import static org.junit.Assert.*;
import static org.hamcrest.core.Is.*;

import org.junit.Test;

public class DigitsTest {

	@Test(timeout = 1000)
	public void testProduct() {

		// assertEquals(6, Digits.sum(123));
		// assertNotEquals(6, Digits.sum(1234));

		assertEquals(24, Digits.product(1234));
		assertNotEquals(6, Digits.product(1234));

		// assertThat(SumOfDigits.sum(123), is(6));
		// can print out error messages
		// comparing floats- use delta value, they are not the same bitwise

	}

	@Test
	public void testProductZero() {
		assertEquals(0, Digits.product(1053532));
		assertEquals(0, Digits.product(0));
	}

	@Test
	public void testProductMax() {
		int megaNumber = 1999999999;
		assertEquals((int) Math.pow(9, String.valueOf(megaNumber).length()), Digits.product(megaNumber));
	}

	@Test
	public void testProductNegative() {
		assertEquals(-10, Digits.product(-25));
		assertEquals(-8, Digits.product(-24));
		assertEquals(-16, Digits.product(-242));
	}

	@Test
	public void testProductElse() {

		assertEquals(0, Digits.product(null));

	}

	@Test
	public void testSum() {
		assertEquals(5, Digits.sum(122));
		assertEquals(0, Digits.sum(null));

	}

}
