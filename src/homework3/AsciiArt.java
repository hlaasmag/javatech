package homework3;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*;

import homework3.imgscaler.Scalr;
import homework3.imgscaler.Scalr.Method;
import homework3.imgscaler.Scalr.Mode;

public class AsciiArt {
	public static void main(String[] args) throws IOException {
		String inputImage = "heart2.jpg";
		File file = new File(inputImage);
		BufferedImage image = ImageIO.read(file);
		BufferedImage scaledImg = Scalr.resize(image, Method.QUALITY, 
                15, 10, Scalr.OP_ANTIALIAS); // Using imagescaler. Copyright 2011 The Buzz Media
		JLabel label = new JLabel(new ImageIcon(scaledImg));
		JFrame f = new JFrame();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.getContentPane().add(label);
		f.pack();
		f.setLocation(200, 200);
		f.setVisible(true);
		
		
		

		int pictureWidth = scaledImg.getWidth();
		int pictureHeight = scaledImg.getHeight();

		int[][] rgbMatrix = createRgbMatrix(pictureHeight, pictureWidth, scaledImg);
		
		printMultilineString(pictureHeight, pictureWidth, rgbMatrix);
	}

	public static int[][] createRgbMatrix(int pictureHeight, int pictureWidth, BufferedImage picture) {
		int[][] rgbMatrix = new int[pictureHeight][pictureWidth];

		for (int i = 0; i < pictureHeight; i++) {
			for (int j = 0; j < pictureWidth; j++) {
				rgbMatrix[i][j] = picture.getRGB(j, i);
			}
		}
		return rgbMatrix;
	}

	public static void printMultilineString(int pictureHeight, int pictureWidth, int[][] rgbMatrix) {
		for (int i = 0; i < pictureHeight; i++) {
			for (int j = 0; j < pictureWidth; j++) {
				Color color = new Color(rgbMatrix[i][j]);
				if (color.equals(Color.BLACK)) {
					System.out.print("*");
				} else {
					System.out.print(".");
				}
			}
			System.out.println();

		}
	}
}

/*
 * JFrame frame = new JFrame("AsciiInput");
 * frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 * 
 * 
 * frame.setVisible(true); //Create components and put them in the frame
 * frame.add(new JLabel(new ImageIcon("heart.lnk")));
 */

/*
 * public static void main(String[] args) { final BufferedImage picture = new
 * BufferedImage(100,100,BufferedImage.SCALE_SMOOTH);
 * 
 * 
 * 
 * try { picture = ImageIO.read(new File("heart.jpg")); } catch (IOException e)
 * { // TODO Auto-generated catch block e.printStackTrace(); } JFrame window =
 * new JFrame() { private static final long serialVersionUID = 1L;
 * 
 * @Override public void paint(Graphics g) { super.paint(g);
 * g.drawImage(picture, 0, 0, null); } };
 * 
 * window.setSize(150, 150); window.setVisible(true); }
 */

// Image image = new Image("file:Morgan.jpg");
// ImageView pildiKuva = new ImageView(image);
// kujundus2.getChildren().addAll(pildiKuva, vaade2to1);

/*
 * 
 * Improve the homework 1 exercise so that it would turn a black-and-white image
 * into an ASCII art. <- input picture file of a heart.. output ->
 * 
 * #### #### ############# ############# ######### ###
 * 
 * If you figure that out easily, improve the program even further:
 * 
 * let user input any image (an URL containing an image); resize images that are
 * too big; accept images of all kinds of colors; maybe add edge detection
 * (Gaussian difference or Sobel), i.e:
 **** 
 * **** *** * ** ***
 ***
 * 
 * 
 */