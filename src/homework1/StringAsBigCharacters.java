package homework1;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.Scanner;

import javax.swing.JFrame;

public class StringAsBigCharacters {

	public static void main(String[] args) {
		String inputUppercase = scanUserInput();
		final BufferedImage picture = new BufferedImage(100, 25, BufferedImage.TYPE_INT_ARGB);
		drawUserInput(inputUppercase, picture);

		JFrame window = new JFrame() {
			private static final long serialVersionUID = 1L;

			@Override
			public void paint(Graphics g) {
				super.paint(g);
				g.drawImage(picture, 0, 0, null);
			}
		};
		
		window.setSize(150, 150);
		window.setVisible(true);

		int pictureWidth = picture.getWidth();
		int pictureHeight = picture.getHeight();

		int[][] rgbMatrix = createRgbMatrix(pictureHeight, pictureWidth, picture);
		window.repaint();
		printMultilineString(pictureHeight, pictureWidth, rgbMatrix);
	}

	public static String scanUserInput() {
		Scanner scan = new Scanner(System.in);
		String inputUppercase = scan.next().toUpperCase();
		scan.close();
		return inputUppercase;
	}

	public static void drawUserInput(String inputUppercase, BufferedImage picture) {
		Graphics graphics = picture.getGraphics();
		graphics.setColor(Color.WHITE);
		graphics.fillRect(0, 0, 200, 200);
		graphics.setColor(Color.BLACK);
		graphics.drawString(inputUppercase, 10, 10);
	}

	public static int[][] createRgbMatrix(int pictureHeight, int pictureWidth, BufferedImage picture) {
		int[][] rgbMatrix = new int[pictureHeight][pictureWidth];

		for (int i = 0; i < pictureHeight; i++) {
			for (int j = 0; j < pictureWidth; j++) {
				rgbMatrix[i][j] = picture.getRGB(j, i);
			}
		}
		return rgbMatrix;
	}

	public static void printMultilineString(int pictureHeight, int pictureWidth, int[][] rgbMatrix) {
		for (int i = 0; i < pictureHeight; i++) {
			for (int j = 0; j < pictureWidth; j++) {
				Color color = new Color(rgbMatrix[i][j]);
				if (color.equals(Color.BLACK)) {
					System.out.print("*");
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();

		}

	}

}
